from django.db import models
from django.forms import ModelForm


# Create your models here.

from django.db import models

class Equipment(models.Model):
    description = models.CharField(max_length=255,unique=True, blank=False, null=False)

    def __str__(self):
        return self.description


class EquipmentForm(ModelForm):
    class Meta:
        model = Equipment
        fields = ['description']