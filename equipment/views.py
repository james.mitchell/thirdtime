from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader

from .models import Equipment, EquipmentForm

def index(request):
    equipment_list = Equipment.objects.all
    template = loader.get_template('equipment/index.html')
    context = {
        'equipment_list': equipment_list,
    }
    return HttpResponse(template.render(context, request))

def create(request, pk=None):
    template = loader.get_template('equipment/create.html')
    form = EquipmentForm(request.POST)
    print('yup')
    if request.method == 'POST' and form.is_valid():
        new_equipmentform = form.save()
        print('yup1')
        return HttpResponseRedirect('/equipment')
    elif request.method == 'POST' and not form.is_valid():
        context = {
        'form': form
        }
        return HttpResponse(template.render(context, request))
    elif pk is not None:
        equipment = Equipment.objects.get(pk=pk)
        form = EquipmentForm(instance=equipment)
        context = {
            'form': form
        }
        return HttpResponse(template.render(context, request))
    else:
        form = EquipmentForm()
        context = {
        'form': form
        }
    return HttpResponse(template.render(context, request))

def delete(request, pk):
    Equipment.objects.filter(id=pk).delete()
    return HttpResponseRedirect('/equipment')