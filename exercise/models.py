from django.db import models
from django.forms import ModelForm

# Create your models here.

from django.db import models

class Exercise(models.Model):
    name =  models.CharField(max_length=255)
    description =  models.CharField(max_length=255)
    testable = models.BooleanField(default='False')

    def __str__(self):
        return self.name

class ExerciseForm(ModelForm):
    class Meta:
        model = Exercise
        fields = ['name', 'description', 'testable']