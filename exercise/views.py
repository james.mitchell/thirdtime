from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader

from .models import Exercise, ExerciseForm

def index(request):
    exercise_list = Exercise.objects.all
    template = loader.get_template('exercise/index.html')
    context = {
        'exercise_list': exercise_list,
    }
    return HttpResponse(template.render(context, request))

def create(request, pk=None):
    template = loader.get_template('exercise/create.html')
    form = ExerciseForm(request.POST)
    print('yup')
    if request.method == 'POST' and form.is_valid():
        new_exerciseform = form.save()
        print('yup1')
        return HttpResponseRedirect('/exercise')
    elif request.method == 'POST' and not form.is_valid():
        context = {
        'form': form
        }
        return HttpResponse(template.render(context, request))
    elif pk is not None:
        exercise = Exercise.objects.get(pk=pk)
        form = ExerciseForm(instance=exercise)
        context = {
            'form': form
        }
        return HttpResponse(template.render(context, request))
    else:
        form = ExerciseForm()
        context = {
        'form': form
        }
    return HttpResponse(template.render(context, request))

def delete(request, pk):
    Exercise.objects.filter(id=pk).delete()
    return HttpResponseRedirect('/exercise')